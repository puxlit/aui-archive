package com.atlassian.aui.test.rest.contacts;

import com.atlassian.aui.test.util.CollectionReorderer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpSession;

/**
 */
public class ContactStore
{

    final static String SESSION_KEY = "RestfulTableContacts";


    public static Set<ContactBean> getContacts(HttpSession session)
    {
        Set<ContactBean> contacts = (LinkedHashSet<ContactBean>) session.getAttribute(SESSION_KEY);

        if (contacts == null)
        {
            return new LinkedHashSet<ContactBean>();
        }
        else
        {

            List<ContactBean> listContacts = new ArrayList<ContactBean>(contacts);
            Collections.reverse(listContacts);
            Set<ContactBean> reversedContacts = new LinkedHashSet<ContactBean>(listContacts);
            return reversedContacts;
        }
    }

    public static Set<ContactBean> getContactsUnformatted(HttpSession session)
    {
        Set<ContactBean> contacts = (LinkedHashSet<ContactBean>) session.getAttribute(SESSION_KEY);
        if (contacts == null)
        {
            return new LinkedHashSet<ContactBean>();
        }
        return contacts;
    }

    public static ContactBean getContactById(HttpSession session, long id)
    {
        Set<ContactBean> contacts = getContactsUnformatted(session);
        for (ContactBean contact : contacts)
        {
            if (contact.getId() == id)
            {
                return contact;
            }
        }
        return null;
    }

    public static Result<ContactBean> validateContact(HttpSession session, final ContactBean contact)
    {
        final Set<ContactBean> contacts = getContactsUnformatted(session);
        final Result.Builder<ContactBean> contactBuilder = new Result.Builder<ContactBean>();

        contactBuilder.setResult(contact);

        for (ContactBean currContact : contacts)
        {
            if (currContact.getName().equals(contact.getName()))
            {
                contactBuilder.addError("name", "You have another contact with the same name");
            }
        }

        if (contact.getNumber() != null)
        {
            try
            {
                Integer.parseInt(contact.getNumber(), 10);
            }
            catch (Exception e)
            {
                contactBuilder.addError("number", "Not a valid number");
            }
        }

        return contactBuilder.build();
    }

    public static ContactBean mergeBeans(ContactBean contact, ContactBean modifications)
    {
        if (modifications.getName() != null)
        {
            contact.setName(modifications.getName());
        }
        if (modifications.getGroup() != null)
        {
            contact.setGroup(modifications.getGroup());
        }
        if (modifications.getNumber() != null)
        {
            contact.setNumber(modifications.getNumber());
        }
        return contact;
    }

    private static ContactBean updateContact(final HttpSession session, final ContactBean contact)
    {
        final Set<ContactBean> contacts = getContactsUnformatted(session);
        contacts.add(contact);
        session.setAttribute(SESSION_KEY, contacts);
        return contact;
    }

    public static void deleteContact(HttpSession session, Long id)
    {
        final Set<ContactBean> contacts = getContactsUnformatted(session);
        final ContactBean contact = getContactById(session, id);
        contacts.remove(contact);
    }

    public static Result<ContactBean> mergeModsIntoContact(HttpSession session, ContactBean contact, final ContactBean modifications)
    {
        final Result<ContactBean> contactResult = validateContact(session, modifications);

        if (contactResult.isValid())
        {
            contact = mergeBeans(contact, contactResult.getResult());
            updateContact(session, contactResult.getResult());
            contactResult.setResult(contact);
        }

        return contactResult;
    }

    public static Result<ContactBean> createNewContactFromBean(HttpSession session, final ContactBean contact)
    {
        final Result<ContactBean> contactResult = validateContact(session, contact);
        if (contactResult.isValid())
        {
            updateContact(session, contactResult.getResult());
        }
        return contactResult;
    }


    public static void increateContactSequence(HttpSession session, long id)
    {
        final CollectionReorderer collectionReorderer = new CollectionReorderer();
        final ContactBean contactBean = getContactById(session, id);
        Set<ContactBean> contacts = getContactsUnformatted(session);
        final ArrayList<ContactBean> contactBeansList = new ArrayList<ContactBean>(contacts);
        collectionReorderer.increasePosition(contactBeansList, contactBean);
        final LinkedHashSet<ContactBean> updatedCollection = new LinkedHashSet<ContactBean>(contactBeansList);
        session.setAttribute(SESSION_KEY, updatedCollection);
    }

    public static void decreaseContactSequence(HttpSession session, long id)
    {
        final CollectionReorderer collectionReorderer = new CollectionReorderer();
        final ContactBean contactBean = getContactById(session, id);
        Set<ContactBean> contacts = getContactsUnformatted(session);
        final ArrayList<ContactBean> contactBeansList = new ArrayList<ContactBean>(contacts);
        collectionReorderer.decreasePosition(contactBeansList, contactBean);
        final LinkedHashSet<ContactBean> updatedCollection = new LinkedHashSet<ContactBean>(contactBeansList);
        session.setAttribute(SESSION_KEY, updatedCollection);
    }


    public static void moveToStartContactSequence(HttpSession session, long id)
    {
        final CollectionReorderer collectionReorderer = new CollectionReorderer();
        final ContactBean contactBean = getContactById(session, id);
        Set<ContactBean> contacts = getContactsUnformatted(session);
        final ArrayList<ContactBean> contactBeansList = new ArrayList<ContactBean>(contacts);
        collectionReorderer.moveToStart(contactBeansList, contactBean);
        final LinkedHashSet<ContactBean> updatedCollection = new LinkedHashSet<ContactBean>(contactBeansList);
        session.setAttribute(SESSION_KEY, updatedCollection);
    }

    public static void moveToEndContactSequence(HttpSession session, long id)
    {
        final CollectionReorderer collectionReorderer = new CollectionReorderer();
        final ContactBean contactBean = getContactById(session, id);
        Set<ContactBean> contacts = getContactsUnformatted(session);
        final ArrayList<ContactBean> contactBeansList = new ArrayList<ContactBean>(contacts);
        collectionReorderer.moveToEnd(contactBeansList, contactBean);
        final LinkedHashSet<ContactBean> updatedCollection = new LinkedHashSet<ContactBean>(contactBeansList);
        session.setAttribute(SESSION_KEY, updatedCollection);
    }

    public static void moveContactAfter(HttpSession session, long id, long afterId)
    {
        final CollectionReorderer collectionReorderer = new CollectionReorderer();
        final ContactBean contactBean = getContactById(session, id);
        final ContactBean afterContact = getContactById(session, afterId);
        Set<ContactBean> contacts = getContactsUnformatted(session);
        final ArrayList<ContactBean> contactBeansList = new ArrayList<ContactBean>(contacts);
        collectionReorderer.moveToPositionAfter(contactBeansList, contactBean, afterContact);
        final LinkedHashSet<ContactBean> updatedCollection = new LinkedHashSet<ContactBean>(contactBeansList);
        session.setAttribute(SESSION_KEY, updatedCollection);
    }

}
