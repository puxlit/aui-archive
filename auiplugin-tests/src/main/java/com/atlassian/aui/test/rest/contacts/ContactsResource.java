package com.atlassian.aui.test.rest.contacts;

import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Produces ({ MediaType.APPLICATION_JSON })
@Consumes ({ MediaType.APPLICATION_JSON })
@Path ("contacts")
public class ContactsResource
{

    CacheControl cacheControl;

    public ContactsResource()
    {
        CacheControl cacheControl = new CacheControl();
        cacheControl.setNoCache(true);
    }

    @GET
    public Response getAllContacts(@Context HttpServletRequest request)
    {
        final HttpSession session = request.getSession(true);
        final Set<ContactBean> contacts = ContactStore.getContacts(session);
        return Response.ok(contacts).cacheControl(cacheControl).build();
    }

    @PUT
    @Path ("{id}")
    public Response updateContact(@PathParam ("id") final long id, @Context HttpServletRequest request, final ContactBean modifications)
    {

        final HttpSession session = request.getSession(true);
        final Set<ContactBean> contacts = ContactStore.getContacts(session);

        for (ContactBean contact : contacts)
        {
            // If you get a class cast exception here. Start a new session! Different classloader when you do atlas-cli...
            if (contact.getId() == id)
            {
                final Result<ContactBean> contactResult = ContactStore.mergeModsIntoContact(session, contact, modifications);
                if (contactResult.isValid())
                {
                    return Response.ok(contactResult.getResult()).build();
                }
                else
                {
                    return Response.status(Response.Status.BAD_REQUEST)
                            .entity(new ErrorCollection(contactResult.getErrors()))
                            .cacheControl(cacheControl).build();
                }
            }
        }
        return Response.ok(Response.Status.BAD_REQUEST).cacheControl(cacheControl).build();
    }

    @POST
    @Path ("/{id}/move")
    public Response movePosition(@PathParam ("id") final Long id, @Context HttpServletRequest request, ContactMoveBean bean)
    {

        final HttpSession session = request.getSession(true);

        // The version can be moved to the top or bottom or after another version
        if (bean.position != null)
        {
            switch (bean.position)
            {
                case Earlier:
                {
                    ContactStore.increateContactSequence(session, id);
                    break;
                }
                case Later:
                {
                    ContactStore.decreaseContactSequence(session, id);
                    break;
                }
                case First:
                {
                    ContactStore.moveToStartContactSequence(session, id);
                    break;
                }
                case Last:
                {
                    ContactStore.moveToEndContactSequence(session, id);
                    break;
                }
            }
        }
        else if (bean.after != null)
        {
            // Get the id from the bean.after URI
            long afterVersionId = getContactIdFromSelfLink(bean.after.getPath());
            ContactStore.moveContactAfter(session, id, afterVersionId);
        }
        return Response.ok().build();
    }

    @POST
    public Response createContact(@Context HttpServletRequest request, final ContactBean contactBean)
    {
        final HttpSession session = request.getSession(true);
        final Result<ContactBean> contactResult = ContactStore.createNewContactFromBean(session, contactBean);
        if (contactResult.isValid())
        {
            return Response.ok(contactBean).build();
        }
        else
        {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(new ErrorCollection(contactResult.getErrors()))
                    .cacheControl(cacheControl)
                    .build();
        }
    }

    @DELETE
    @Path ("/{id}")
    public Response deleteContact(@PathParam ("id") final Long id, @Context HttpServletRequest request)
    {
        final HttpSession session = request.getSession(true);
        ContactStore.deleteContact(session, id);
        return Response.ok().build();
    }

    private long getContactIdFromSelfLink(String path)
    {
        String contactIdString = path.substring(path.lastIndexOf('/') + 1);
        long afterContactId = -1;
        afterContactId = Long.parseLong(contactIdString);
        return afterContactId;
    }
}
