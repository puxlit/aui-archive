module("eve.js Unit Tests");

// Note: eve will be required by any path including "evejs-unit-tests"
test("Check if Eve can be loaded", function() {
    var eveIsDefined = (typeof eve == 'undefined') ? false : true;
    ok(eveIsDefined, " Eve has not loaded. ");        
});
