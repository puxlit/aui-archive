(function () {
var fixture, dropdown;
module("Dropdown Unit Tests", {
    setup: function () {
        fixture = AJS.$("#qunit-fixture").html(
            "<ul id=\"dropdown-test\">" +
            "    <li class=\"aui-dd-parent\">" +
            "       <a href=\"#\" class=\"aui-dd-trigger\">A Test Dropdown</a>" +
            "       <ul class=\"aui-dropdown\">" +
            "           <li class=\"dropdown-item\"><a href=\"#\" class=\"item-link\">Link 1</a></li>" +
            "           <li class=\"dropdown-item\"><a href=\"#\" class=\"item-link\">Link 2</a></li>" +
            "           <li class=\"dropdown-item\"><a href=\"#\" class=\"item-link\">Link 3</a></li>" +
            "       </ul>" +
            "    </li>" +
            "</ul>");
        dropdown = AJS.$("#dropdown-test").dropDown("Standard")[0];
    },
    teardown: function () {
        dropdown.hide();
    }
});

test("Dropdown Creation", function() {
    var testDropdown = AJS.dropDown("test","standard");
    ok(typeof testDropdown == "object", "dropdown object was created successfully!");
});

test("Dropdown move down", function() {
    AJS.$("#dropdown-test .aui-dd-trigger").click();
    dropdown.moveDown();
    var dropdownItems = AJS.$(".dropdown-item"),
        selectedItem = AJS.$(".dropdown-item.active");
    equals(selectedItem.length, 1, "Only one item selected");
    equals(selectedItem[0], dropdownItems[0], "First item selected");

    dropdown.cleanActive();
    dropdown.moveDown();
    selectedItem = AJS.$(".dropdown-item.active");
    equals(selectedItem.length, 1, "Only one item selected");
    equals(selectedItem[0], dropdownItems[1], "Second item selected");

    dropdown.cleanActive();
    dropdown.moveDown();
    selectedItem = AJS.$(".dropdown-item.active");
    equals(selectedItem.length, 1, "Only one item selected");
    equals(selectedItem[0], dropdownItems[2], "Third item selected");
    
    dropdown.cleanActive();
    dropdown.moveDown();
    selectedItem = AJS.$(".dropdown-item.active");
    equals(selectedItem.length, 1, "Only one item selected");
    equals(selectedItem[0], dropdownItems[0], "First item selected again");
});

test("Dropdown move up", function() {
    AJS.$("#dropdown-test .aui-dd-trigger").click();
    dropdown.moveUp();
    var dropdownItems = AJS.$(".dropdown-item"),
        selectedItem = AJS.$(".dropdown-item.active");
    equals(selectedItem.length, 1, "Only one item selected");    
    equals(selectedItem[0], dropdownItems[2], "Third item selected");

    dropdown.cleanActive();
    dropdown.moveUp();
    selectedItem = AJS.$(".dropdown-item.active");
    equals(selectedItem.length, 1, "Only one item selected");
    equals(selectedItem[0], dropdownItems[1], "Second item selected");

    dropdown.cleanActive(); 
    dropdown.moveUp();
    selectedItem = AJS.$(".dropdown-item.active");
    equals(selectedItem.length, 1, "Only one item selected");
    equals(selectedItem[0], dropdownItems[0], "First item selected");
    
    dropdown.cleanActive(); 
    dropdown.moveUp();
    selectedItem = AJS.$(".dropdown-item.active");
    equals(selectedItem.length, 1, "Only one item selected");
    equals(selectedItem[0], dropdownItems[2], "Third item selected again");
});
})();
