(function ($) {


    // We have custom renders for the edit row so we must define them here
    var EditRow = AJS.RestfulTable.EditRow.extend({

        // create renderer for group column
        renderGroup: function (self, all) {

            var $select = $("<select name='group'>" +
                "<option value='Friends'>Friends</option>" +
                "<option value='Family'>Family</option>" +
                "<option value='Work'>Work</option>" +
            "</select>");

            $select.val(self.value); // select currently selected

            return $select;
        }
    });

    // DOM ready
    $(function () {

        var url = AJS.contextPath() + "/rest/contacts/1.0/contacts",
            $contactsTable = $("#contacts-table");

        new AJS.RestfulTable({
            el: $contactsTable, // <table>
            autofocus: true, // auto focus first field of create row
            columns: [
                {id: "name", header: "Name"}, // id is the mapping of the rest property to render
                {id: "group", header: "Group"}, // header is the text in the <th>
                {id: "number", header: "Number"}
            ],
            resources: {
                all: url, // resource to get all contacts
                self: url // resource to get single contact url/{id}
            },
            noEntriesMsg: "You have no contacts. Loner!", // message to be displayed when table is empty
            reorderable: true, // drag and drop reordering
            views: {
                editRow: EditRow
            }

        });
    });

})(AJS.$);