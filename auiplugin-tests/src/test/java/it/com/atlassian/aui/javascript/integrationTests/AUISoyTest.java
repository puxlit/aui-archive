package it.com.atlassian.aui.javascript.integrationTests;

/**
 * @since 3.7
 */
public class AUISoyTest extends AbstractAUISeleniumTestCase
{
    private static final String TEST_PAGE = "test-pages/soy/soy-test.html";

    public void testSoyWorks()
    {
        openTestPage(TEST_PAGE);

        assertThat.elementPresent("css=div#soy-testing");
        assertThat.elementContainsText("css=div#soy-testing", "Soy loaded");
    }
}
