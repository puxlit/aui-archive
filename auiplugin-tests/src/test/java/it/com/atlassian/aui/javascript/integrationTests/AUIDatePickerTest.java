package it.com.atlassian.aui.javascript.integrationTests;

public class AUIDatePickerTest extends AbstractAUISeleniumTestCase {

    private static final String TEST_PAGE = "test-pages/experimental/date-picker/date-picker-test.html";

    public void testDatePickerPopupDisplaysOnFieldFocus() {

        openTestPage(TEST_PAGE);

        // click the field and wait for the date picker to appear
        client.click("jquery=#test-basic-always");
        assertThat.elementPresentByTimeout("jquery=.aui-inline-dialog:visible > .contents > .hasDatepicker");

    }

//    // doesn't seem to work; not sure why...
//    public void testDatePickerPopupHidesOnExternalClick() {
//
//        openTestPage(TEST_PAGE);
//
//        // click the field and wait for the date picker to appear
//        client.click("jquery=#test-basic-always");
//        assertThat.elementPresentByTimeout("jquery=.aui-inline-dialog:visible > .contents > .hasDatepicker");
//
//        // click on the page heading and wait for the date picker to disappear
//        client.click("jquery=.navigation h2");
//        assertThat.elementNotPresentByTimeout("jquery=.aui-inline-dialog:visible > .contents > .hasDatepicker");
//
//    }

    public void testDatePickerPopulatesFieldOnSelection() {

        openTestPage(TEST_PAGE);

        // click the field and wait for the date picker to appear
        client.click("jquery=#test-default-always");
        assertThat.elementPresentByTimeout("jquery=.aui-inline-dialog:visible > .contents > .hasDatepicker");

        // click the 31st and ensure the field is focused and changes to "2012-01-31"
        client.click("jquery=.aui-inline-dialog:visible td > a.ui-state-default:contains(31)");
        assertThat.elementPresentByTimeout("jquery=#test-default-always:focus[value=2012-01-31]");

    }

    public void testDatePickerInitiallySelectsDefaultValue() {

        openTestPage(TEST_PAGE);

        // click the field and wait for the date picker to appear
        client.click("jquery=#test-default-always");
        assertThat.elementPresentByTimeout("jquery=.aui-inline-dialog:visible > .contents > .hasDatepicker");

        // ensure the date picker shows the 1st of January 2012 as the selected date
        assertThat.elementContainsText("jquery=.aui-inline-dialog:visible span.ui-datepicker-month", "January");
        assertThat.elementContainsText("jquery=.aui-inline-dialog:visible span.ui-datepicker-year", "2012");
        assertThat.elementContainsText("jquery=.aui-inline-dialog:visible td.ui-datepicker-current-day > a.ui-state-active", "1");

    }

    public void testDatePickerDisplaysCorrectRange() {

        openTestPage(TEST_PAGE);

        // click the field and wait for the date picker to appear
        client.click("jquery=#test-range-always");
        assertThat.elementPresentByTimeout("jquery=.aui-inline-dialog:visible > .contents > .hasDatepicker");

        // if necessary, flip to the first page
        if (client.getText("jquery=.aui-inline-dialog:visible span.ui-datepicker-month").equals("January") && client.getText("jquery=.aui-inline-dialog:visible span.ui-datepicker-year").equals("2012")) {
            client.click("jquery=.aui-inline-dialog:visible .ui-datepicker-prev");
        }

        // ensure the date picker is displaying the December 2011 page, the 24th is disabled, and the 25th is enabled
        assertThat.elementPresentByTimeout("jquery=.aui-inline-dialog:visible span.ui-datepicker-month:contains(December)");
        assertThat.elementPresentByTimeout("jquery=.aui-inline-dialog:visible span.ui-datepicker-year:contains(2011)");
        assertThat.elementPresent("jquery=.aui-inline-dialog:visible td.ui-datepicker-unselectable > span.ui-state-default:contains(24)");
        assertThat.elementPresent("jquery=.aui-inline-dialog:visible td > a.ui-state-default:contains(25)");

        // flip to the second page
        client.click("jquery=.aui-inline-dialog:visible .ui-datepicker-next");

        // ensure the date picker is displaying the January 2012 page, the 5th is enabled, and the 6th is disabled
        assertThat.elementPresentByTimeout("jquery=.aui-inline-dialog:visible span.ui-datepicker-month:contains(January)");
        assertThat.elementPresentByTimeout("jquery=.aui-inline-dialog:visible span.ui-datepicker-year:contains(2012)");
        assertThat.elementPresent("jquery=.aui-inline-dialog:visible td > a.ui-state-default:contains(5)");
        assertThat.elementPresent("jquery=.aui-inline-dialog:visible td.ui-datepicker-unselectable > span.ui-state-default:contains(6)");

    }

    public void testDatePickerRespectsCustomFirstDay() {

        openTestPage(TEST_PAGE);

        // click the field and wait for the date picker to appear
        client.click("jquery=#test-start-always");
        assertThat.elementPresentByTimeout("jquery=.aui-inline-dialog:visible > .contents > .hasDatepicker");

        // ensure the date picker is displaying the January 2012 page, and Wednesday is the first day of the week
        assertThat.elementPresentByTimeout("jquery=.aui-inline-dialog:visible span.ui-datepicker-month:contains(January)");
        assertThat.elementPresentByTimeout("jquery=.aui-inline-dialog:visible span.ui-datepicker-year:contains(2012)");
        assertThat.elementPresent("jquery=.aui-inline-dialog:visible thead th:nth(0) > span:contains(Wed)");
        assertThat.elementPresent("jquery=.aui-inline-dialog:visible thead th:nth(1) > span:contains(Thu)");
        assertThat.elementPresent("jquery=.aui-inline-dialog:visible thead th:nth(2) > span:contains(Fri)");
        assertThat.elementPresent("jquery=.aui-inline-dialog:visible thead th:nth(3) > span:contains(Sat)");
        assertThat.elementPresent("jquery=.aui-inline-dialog:visible thead th:nth(4) > span:contains(Sun)");
        assertThat.elementPresent("jquery=.aui-inline-dialog:visible thead th:nth(5) > span:contains(Mon)");
        assertThat.elementPresent("jquery=.aui-inline-dialog:visible thead th:nth(6) > span:contains(Tue)");

    }

    public void testDatePickerRespectsCustomLocale() {

        openTestPage(TEST_PAGE);

        // click the field and wait for the date picker to appear
        client.click("jquery=#test-lang-always");
        assertThat.elementPresentByTimeout("jquery=.aui-inline-dialog:visible > .contents > .hasDatepicker");

        // ensure the date picker is displaying the January 2012 page in French
        assertThat.elementPresentByTimeout("jquery=.aui-inline-dialog:visible span.ui-datepicker-month:contains(Janvier)");
        assertThat.elementPresentByTimeout("jquery=.aui-inline-dialog:visible span.ui-datepicker-year:contains(2012)");
        assertThat.elementPresent("jquery=.aui-inline-dialog:visible thead th:nth(0) > span:contains(Lun.)");
        assertThat.elementPresent("jquery=.aui-inline-dialog:visible thead th:nth(1) > span:contains(Mar.)");
        assertThat.elementPresent("jquery=.aui-inline-dialog:visible thead th:nth(2) > span:contains(Mer.)");
        assertThat.elementPresent("jquery=.aui-inline-dialog:visible thead th:nth(3) > span:contains(Jeu.)");
        assertThat.elementPresent("jquery=.aui-inline-dialog:visible thead th:nth(4) > span:contains(Ven.)");
        assertThat.elementPresent("jquery=.aui-inline-dialog:visible thead th:nth(5) > span:contains(Sam.)");
        assertThat.elementPresent("jquery=.aui-inline-dialog:visible thead th:nth(6) > span:contains(Dim.)");

    }

}
